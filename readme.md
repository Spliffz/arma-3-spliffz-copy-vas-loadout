Spliffz Gear Copy Script
Arma 3 
2013 - Spliffz <thespliffz@gmail.com


To get the loadout you made with VAS so you can use it in other missions that has no VAS.


[Installation]
Copy the folder to your mission directory.
Add this line to your init.sqf:
null = [] execVM "SpliffzCopyVasLoadout\spliffz_copyGear.sqf";


[How to use]
With this scripts loaded you will find a couple of actions in your action(scroll) menu.
Pick some gear, click the action and check your .RPT log file (%APPDATA%\local\arma 3\arma3_xx_xx_xx_xx.rpt)
It puts down the classnames or array with classnames, which can be copy/pasted in your own scripts.



// EOF