/**
  * Spliffz Gear Copy Script
  * to get the loadout you made with VAS so you can use it in other missions that has no VAS
  * SpliffzCopyVasLoadout\spliffz_copyGear.sqf
  * Arma 3 - Spliffz <thespliffz@gmail.com
**/

// alles doen op de player, localy.
if(isDedicated) exitWith {};


player addAction ["Copy Uniform", "SpliffzCopyVasLoadout\spliffz_copy2clipboard.sqf", "uniform"];
player addAction ["Copy Uniform Items", "SpliffzCopyVasLoadout\spliffz_copy2clipboard.sqf", "uniformItems"];
player addAction ["Copy Vest", "SpliffzCopyVasLoadout\spliffz_copy2clipboard.sqf", "vest"];
player addAction ["Copy Vest Items", "SpliffzCopyVasLoadout\spliffz_copy2clipboard.sqf", "vestItems"];
player addAction ["Copy Headgear", "SpliffzCopyVasLoadout\spliffz_copy2clipboard.sqf", "headgear"];
player addAction ["Copy Goggles", "SpliffzCopyVasLoadout\spliffz_copy2clipboard.sqf", "goggles"];
player addAction ["Copy Backpack", "SpliffzCopyVasLoadout\spliffz_copy2clipboard.sqf", "backpack"];
player addAction ["Copy Backpack Items", "SpliffzCopyVasLoadout\spliffz_copy2clipboard.sqf", "backpackItems"];
player addAction ["Copy Weapons", "SpliffzCopyVasLoadout\spliffz_copy2clipboard.sqf", "weapons"];
player addAction ["Copy Weapon Attachments", "SpliffzCopyVasLoadout\spliffz_copy2clipboard.sqf", "wpnAttachments"];
player addAction ["Copy Pistol Attachments", "SpliffzCopyVasLoadout\spliffz_copy2clipboard.sqf", "pistolAttachments"];
player addAction ["Copy Items", "SpliffzCopyVasLoadout\spliffz_copy2clipboard.sqf", "items"];
player addAction ["Copy Assigned Items", "SpliffzCopyVasLoadout\spliffz_copy2clipboard.sqf", "assItems"];
player addAction ["Copy Sec. Wpn Attachments", "SpliffzCopyVasLoadout\spliffz_copy2clipboard.sqf", "secwpnAttachments"];

// EOF