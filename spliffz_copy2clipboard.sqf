/**
  * Spliffz Gear Copy Script
  * to get the loadout you made with VAS so you can use it in other missions that has no VAS
  * SpliffzCopyVasLoadout\spliffz_copy2clipboard.sqf
  * Arma 3 - Spliffz <thespliffz@gmail.com
**/

// alles doen op de player, localy.
if(isDedicated) exitWith {};

private ["_switch", "_uniform", "_uniformItems", "_vest", "_vestItems", "_headgear", "_backpack", "_weapons", "_wpnAttachments", "_pistolAttachments", "_items", "_assignedItems", "_secWpnAttachments", "_ammo"];

_switch = _this select 3;
//_switchItem = _switch select 0;

switch (_switch) do {
		case "uniform": {
			_uniform = uniform player;
			copyToClipboard str _uniform;
		};
		
		case "uniformItems": {
			_uniformItems = uniformItems player;
			copyToClipboard str _uniformItems;
		};

		case "vest": {
			_vest = vest player;
			copyToClipboard str _vest;
		};

		case "vestItems": {
			_vestItems = vestItems player;
			copyToClipboard str _vestItems;
		};
		
		case "headgear": {
			_headgear = headgear player;
			copyToClipboard str _headgear;
		};
		
		case "goggles": {
			_goggles = goggles player;
			copyToClipboard str _goggles;
		};
		
		case "backpack": {
			_backpack = backpack player;
			copyToClipboard str _backpack;
		};
		
		case "backpackItems": {
			_backpackItems = backpackItems player;
			copyToClipboard str _backpackItems;
		};
		
		case "weapons": {
			_weapons = weapons player;
			copyToClipboard str _weapons;
		};
		
		case "wpnAttachments": {
			_wpnAttachments = primaryWeaponItems player;
			copyToClipboard str _wpnAttachments;
		};
		
		case "pistolAttachments": {
			_pistolAttachments = handgunItems player;
			copyToClipboard str _pistolAttachments;
		};
		
		case "items": {
			_items = items player;
			copyToClipboard str _items;
		};
		
		case "assItems": {
			_assItems = assignedItems player;
			copyToClipboard str _assItems;
		};
		
		case "secwpnAttachments": {
			_secWpnAttachments = secondaryWeaponItems player;
			copyToClipboard str _secWpnAttachments;
		};
		
		case "ammo": {
			_ammo = magazines player;
			copyToClipboard str _ammo;
		};
};

Hint "You can now tab out and copy/paste this to the your script file witch ctrl+v.";

// EOF